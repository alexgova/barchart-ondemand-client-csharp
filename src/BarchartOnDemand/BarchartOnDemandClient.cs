﻿using BarchartOnDemand.Api;
using BarchartOnDemand.Responses;
using System;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using Newtonsoft.Json;

namespace BarchartOnDemand {

    public class BarchartOnDemandClient {

        public string ApiKey { get; set; }
        public string BaseUrl { get; set; } = "http://ondemand.websol.barchart.com/";
        public bool Debug { get; set; }

        private HttpClient httpClient = new HttpClient();

        public T Fetch<T>(IOnDemandRequest request) where T : ResponseBase {

            if(string.IsNullOrEmpty(ApiKey)) {
                throw new ArgumentException("ApiKey is required");
            }

            if(request == null) {
                throw new ArgumentNullException("request cannot be null");
            }

            var requestUri = BuildRequestUrl(request);

            var result = httpClient.GetStringAsync(requestUri).Result;

            return JsonConvert.DeserializeObject<T>(result);
        }

        private string BuildRequestUrl(IOnDemandRequest request) {
            var sb = new StringBuilder();

            sb.Append(BaseUrl);
            sb.Append(request.Endpoint);
            sb.Append("?");
            sb.Append(string.Join("&", request.Parameters.Select(x => HttpUtility.UrlEncode(x.Key) + "=" + HttpUtility.UrlEncode(x.Value.ToString()))));
            sb.Append("&apikey=");
            sb.Append(ApiKey);

            return sb.ToString();
        }


    }
}
