﻿using System.Collections.Generic;

namespace BarchartOnDemand.Responses {

    public class QuotesResponse : ResponseBase {

        public IList<Quote> results { get; set; } = new List<Quote>();

    }
}
