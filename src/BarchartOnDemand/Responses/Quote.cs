﻿using System.Collections.Generic;

namespace BarchartOnDemand.Responses {

    public class Quote {

        public string symbol { get; set; }

        public string name { get; set; }

        public string dayCode { get; set; }

        public string serverTimestamp { get; set; }

        public string mode { get; set; }

        public double lastPrice { get; set; }

        public double last { get; set; }

        public string tradeTimestamp { get; set; }

        public double netChange { get; set; }

        public double percentChange { get; set; }

        public string unitCode { get; set; }

        public double open { get; set; }

        public double high { get; set; }

        public double low { get; set; }

        public double close { get; set; }

        public double settlement { get; set; }

        public double previousClose { get; set; }

        public long numTrades { get; set; }

        public double dollarVolume { get; set; }

        public string flag { get; set; }

        public long volume { get; set; }

        public long previousVolume { get; set; }

        public double fiftyTwoWkHigh { get; set; }

        public string fiftyTwoWkHighDate { get; set; }

        public double fiftyTwoWkLow { get; set; }

        public string fiftyTwoWkLowDate { get; set; }

        public Dictionary<string, object> additionalProperties { get; set; } = new Dictionary<string, object>();

    }

}
