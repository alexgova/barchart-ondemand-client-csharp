﻿namespace BarchartOnDemand.Responses {

    public class EtfConstituent {

        public string symbol { get; set; }

        public string name { get; set; }

        public decimal holdingsPercent { get; set; }

        public long sharesHeld { get; set; }

    }
}