﻿using System.Collections.Generic;

namespace BarchartOnDemand.Responses {

    public class SectorsResponse : ResponseBase {

        public IList<Sector> results { get; set; } = new List<Sector>();

    }
}
