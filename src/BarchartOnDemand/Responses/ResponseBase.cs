﻿using System;
using System.Collections.Generic;

namespace BarchartOnDemand.Responses {

    public class ResponseBase : IOnDemandResponse {

        protected Dictionary<string, object> status = new Dictionary<string, object>();

        private static readonly string RESPONSE_EMPTY = "Success, but no content to return.";

        public bool IsEmpty() {
            if (status == null)
                return true;

            if (status.ContainsKey("message") && ((string)status["message"]).Equals(RESPONSE_EMPTY, StringComparison.InvariantCultureIgnoreCase))
                return true;

            return false;
        }

    }
}
