﻿using System;
using System.Collections.Generic;

namespace BarchartOnDemand.Responses {

    public class Sector {

        public string symbol { get; set; }

        public string sector { get; set; }

        public decimal weightAlpha { get; set; }

        public decimal lastTrade { get; set; }

        public decimal change { get; set; }

        public decimal percentChange { get; set; }

        public int? stocks { get; set; }

        public decimal open { get; set; }

        public decimal high { get; set; }

        public decimal low { get; set; }

        public decimal close { get; set; }

        public decimal previousClose { get; set; }

        public decimal fiftyTwoWeekHigh { get; set; }

        public decimal fiftyTwoWeekLow { get; set; }

        public List<SectorComponent> components { get; set; }

        public string componentSymbol { get; set; }

        public string componentName { get; set; }

        public DateTime timestamp { get; set; }

    }
}
