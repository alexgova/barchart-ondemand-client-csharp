﻿namespace BarchartOnDemand.Responses {

    public interface IOnDemandResponse {

        bool IsEmpty();

    }
}
