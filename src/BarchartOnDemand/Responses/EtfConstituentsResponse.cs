﻿using System.Collections.Generic;

namespace BarchartOnDemand.Responses {

    public class EtfConstituentsResponse : ResponseBase {

        public List<EtfConstituent> results { get; set; } = new List<EtfConstituent>();

    }
}
