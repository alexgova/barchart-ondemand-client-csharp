﻿using System;
using System.Collections.Generic;

namespace BarchartOnDemand.Api {

    public class QuoteRequest : IOnDemandRequest {

        public string Symbols { get; set; }
        public string Fields { get; set; }
        public string Mode { get; set; } = "I";
        public string Endpoint => "getQuote.json";
        public string Name => "Quote";

        private Dictionary<string, object> _params = new Dictionary<string, object>();

        public Dictionary<string, object> Parameters {
            get {
                _params.Add("symbols", Symbols);

                if (!String.IsNullOrEmpty(Fields)) {
                    _params.Add("fields", Fields);
                }

                _params.Add("mode", Mode);

                return _params;
            }
        }
    }
}
