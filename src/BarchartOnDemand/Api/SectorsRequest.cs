﻿using System.Collections.Generic;

namespace BarchartOnDemand.Api {

    public class SectorsRequest : IOnDemandRequest {

        public string Sectors { get; set; } = string.Empty;
        public string SectorPeriod { get; set; } = "1DA";
        public bool DailyData { get; set; }
        public bool Components { get; set; }
        public string SortDir { get; set; }
        public string Endpoint => "getSectors.json";
        public string Name => "Sectors";

        private Dictionary<string, object> _params = new Dictionary<string, object>();

        public Dictionary<string, object> Parameters {
            get {
                _params.Clear();

                _params.Add("sector", Sectors);

                _params.Add("sectorPeriod", SectorPeriod);

                _params.Add("dailyData", value: DailyData ? "true" : "false");

                _params.Add("components", value: Components ? "true" : "false");

                if (!string.IsNullOrEmpty(SortDir))
                    _params.Add("sortDir", SortDir);

                return _params;
            }
        }
    }
}
