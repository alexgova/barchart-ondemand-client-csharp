﻿using System.Collections.Generic;

namespace BarchartOnDemand.Api {

    public interface IOnDemandRequest {

        string Endpoint { get; }

        string Name { get; }

        Dictionary<string, object> Parameters { get; }

    }
}
