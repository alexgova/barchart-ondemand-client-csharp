﻿using System.Collections.Generic;

namespace BarchartOnDemand.Api {

    public class EtfConstituensRequest : IOnDemandRequest {

        public string Symbol { get; set; }

        public string Endpoint => "getETFConstituents.json";

        public string Name => "ETFConstituents";

        private Dictionary<string, object> _params = new Dictionary<string, object>();

        public Dictionary<string, object> Parameters {
            get {
                _params.Clear();

                _params.Add("symbol", Symbol);

                return _params;
            }
        }
    }
}
