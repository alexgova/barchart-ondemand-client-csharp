﻿using System.Collections.Generic;

namespace BarchartOnDemand.DTOs {

    public class SectorsDto {

        public Dictionary<string, SectorDto> Sectors { get; set; }

    }
}
