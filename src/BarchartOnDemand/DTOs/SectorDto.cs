﻿using System;
using System.Collections.Generic;

namespace BarchartOnDemand.DTOs {

    public class SectorDto {

        public string Sector { get; set; }

        public DateTime? DateAdded { get; set; }

        public DateTime? DateDeleted { get; set; }

        public Dictionary<string, ComponentDto> Components {get; set;}

    }
}