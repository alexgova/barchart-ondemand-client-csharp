﻿using System;

namespace BarchartOnDemand.DTOs {

    public class ComponentDto {

        public string Name { get; set; }

        public string Exchange { get; set; }

        public DateTime? DateAdded { get; set; }

        public DateTime? DateDeleted { get; set; }
    }
}