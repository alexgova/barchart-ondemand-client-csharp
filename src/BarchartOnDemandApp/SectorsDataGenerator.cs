﻿using BarchartOnDemand;
using BarchartOnDemand.Api;
using BarchartOnDemand.DTOs;
using BarchartOnDemand.Responses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Newtonsoft.Json;


namespace BarchartOnDemandApp {
    internal class SectorsDataGenerator : IDataGenerator {

        public BarchartOnDemandClient BarchartClient { get; set; }

        public IOnDemandRequest Request { get; set; }

        public void GenerateData() {

            var sectors = BarchartClient.Fetch<SectorsResponse>(Request).results;

            var sb = new StringBuilder();

            var csvHeader = "Symbol,Sector,WeightAlpha,LastTrade,Change,PercentChange,Stocks,Open,High,Low,Close,PreviousClose,FiftyTwoWeekHigh,FiftyTwoWeekLow";
            var componentsHeader = "Components,Symbol,Name,Exchange";

            sb.AppendLine(csvHeader);

            var downloadedSectors = new SectorsDto {
                Sectors = new Dictionary<string, SectorDto>()
            };

            foreach (var s in sectors) {
                sb.AppendLine($"'{s.symbol},\"{s.sector}\",{s.weightAlpha},{s.lastTrade},{s.change},{s.percentChange},{s.stocks},{s.open},{s.high},{s.low},{s.close},{s.previousClose},{s.fiftyTwoWeekHigh},{s.fiftyTwoWeekLow}");
                sb.AppendLine(componentsHeader);

                if (!downloadedSectors.Sectors.ContainsKey(s.symbol)) {
                    downloadedSectors.Sectors.Add(s.symbol, new SectorDto {
                        Sector = s.sector,
                        Components = new Dictionary<string, ComponentDto>(),
                        DateAdded = DateTime.Today
                    });
                }

                foreach (var c in s.components) {
                    sb.AppendLine($",\"{c.symbol}\",\"{c.name}\",{c.exchange}");

                    downloadedSectors.Sectors[s.symbol].Components.Add(c.symbol, new ComponentDto {
                        Name = c.name,
                        Exchange = c.exchange,
                        DateAdded = DateTime.Today
                    });
                }

            }

            File.WriteAllText($".\\SectorsData_{DateTime.Today.ToString("yyyy-MM-dd")}.csv", sb.ToString());

            UpdateSectorsDb(downloadedSectors);
        }

        private void UpdateSectorsDb(SectorsDto downloadedSectors) {

            var dbFileName = ".\\sectorsdb.json";
            var savedSectors = new SectorsDto();

            if (File.Exists(dbFileName)) {
                savedSectors = JsonConvert.DeserializeObject<SectorsDto>(File.ReadAllText(dbFileName));

                foreach (var sector in savedSectors.Sectors) {
                    if (downloadedSectors.Sectors.ContainsKey(sector.Key)) {
                        var downloadedSector = downloadedSectors.Sectors[sector.Key];
                        var sectorComponents = sector.Value.Components;
                        foreach (var component in sectorComponents) {
                            if (downloadedSector.Components.ContainsKey(component.Key)) {
                                downloadedSector.Components.Remove(component.Key);
                            } else {
                                component.Value.DateDeleted = DateTime.Today;
                            }
                        }
                        foreach (var component in downloadedSector.Components) {
                            sectorComponents.Add(component.Key, component.Value);
                        }
                        downloadedSectors.Sectors.Remove(sector.Key);
                    } else {
                        sector.Value.DateDeleted = DateTime.Today;
                    }
                }

                foreach (var sector in downloadedSectors.Sectors) {
                    savedSectors.Sectors.Add(sector.Key, sector.Value);
                }

            } else {
                savedSectors = downloadedSectors;

            }

            File.WriteAllText(dbFileName, JsonConvert.SerializeObject(downloadedSectors));

            ConvertSavedSectorsToCsv(savedSectors);

        }

        private void ConvertSavedSectorsToCsv(SectorsDto savedSectors) {
            var sb = new StringBuilder();

            foreach (var sector in savedSectors.Sectors) {
                var dateAdded = sector.Value.DateAdded != null ? sector.Value.DateAdded.Value.ToShortDateString() : String.Empty;
                var dateDeleted = sector.Value.DateDeleted != null ? sector.Value.DateDeleted.Value.ToShortDateString() : String.Empty;

                sb.AppendLine($"Symbol:,'{sector.Key}");
                sb.AppendLine($"Sector:,{sector.Value.Sector}");
                sb.AppendLine($"Date Added:,{dateAdded}");
                sb.AppendLine($"Date Deleted:,{dateDeleted}");
                sb.AppendLine("Symbol,Name,Exchange,Date Added,Date Deleted");

                foreach (var component in sector.Value.Components) {
                    dateAdded = component.Value.DateAdded != null ? component.Value.DateAdded.Value.ToShortDateString() : String.Empty;
                    dateDeleted = component.Value.DateDeleted != null ? component.Value.DateDeleted.Value.ToShortDateString() : String.Empty;
                    sb.AppendLine($"{component.Key},{component.Value.Name},{component.Value.Exchange},{dateAdded},{dateDeleted}");
                }
            }

            File.WriteAllText(".\\SectorsDb.csv", sb.ToString());
        }
    }
}