﻿using BarchartOnDemand;
using BarchartOnDemand.Api;

namespace BarchartOnDemandApp {

    internal interface IDataGenerator {

        BarchartOnDemandClient BarchartClient { get; set; }

        IOnDemandRequest Request { get; set; }


        
        void GenerateData();

    }
}