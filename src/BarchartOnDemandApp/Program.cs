﻿using BarchartOnDemand;
using BarchartOnDemand.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BarchartOnDemandApp {

    public class Program {

        private static string BarchartApiKey = "7e5f8eb2a836a5494ff795bfc90475d6";

        private static List<IDataGenerator> DataGenerators = new List<IDataGenerator>();
        private static BarchartOnDemandClient BarchartClient = new BarchartOnDemandClient() { ApiKey = BarchartApiKey };

        public static void Main(string[] args) {

            var sectorsRequest = new SectorsRequest() {
                Sectors = "",
                SectorPeriod = "1DA",
                DailyData = true,
                Components = true,
                SortDir = "ASC"
            };

            var sectorsDataGenerator = new SectorsDataGenerator() {
                BarchartClient = BarchartClient,
                Request = sectorsRequest
            };

            DataGenerators.Add(sectorsDataGenerator);
            //DataGenerators.Add(new EtfConstituentsDataGenerator() { BarchartClient = BarchartClient });

            foreach (var dataGenerator in DataGenerators) {
                dataGenerator.GenerateData();
            }
        }
    }
}
