﻿using BarchartOnDemand;
using BarchartOnDemand.Api;
using BarchartOnDemand.Responses;
using System;
using System.IO;
using System.Text;
using Xunit;
using Xunit.Abstractions;

namespace BarchartOnDemandTest {

    public class ClientTest {

        private BarchartOnDemandClient client;
        private string ApiKey = "7e5f8eb2a836a5494ff795bfc90475d6";
        private readonly ITestOutputHelper output;

        public ClientTest(ITestOutputHelper output) {
            this.output = output;

            client = new BarchartOnDemandClient() {
                ApiKey = ApiKey,
                Debug = true
            };
        }

        [Fact]
        public void CanGetQuote() {
            var request = new QuoteRequest() {
                Symbols = "AAPL,GOOG",
                Mode = "I",
                Fields = "fiftyTwoWkHighDate"
            };

            var quotes = client.Fetch<QuotesResponse>(request).results;

            foreach (var q in quotes) {
                Console.WriteLine("Quote for: " + q.symbol + " = " + q);
            }
        }

        [Fact]
        public void CanGetSectors() {
            var request = new SectorsRequest() {
                Sectors = "-SOLA",
                SectorPeriod = "1DA",
                DailyData = true,
                Components = true,
                SortDir = "ASC"
            };

            var sectors = client.Fetch<SectorsResponse>(request).results;

            foreach (var s in sectors) {
                output.WriteLine("Data for symbol: " + s.symbol + " = " + s);
            }

        }

        [Fact]
        public void CanGetEtfConstituents() {
            var request = new EtfConstituensRequest() {
                Symbol = "SPY"
            };

            var consts = client.Fetch<EtfConstituentsResponse>(request).results;

            var sb = new StringBuilder();
            var csvHeader = "Symbol,Name,Holdings Percent,Shares Held";

            sb.AppendLine(csvHeader);

            foreach (var c in consts) {
                sb.AppendLine($"{c.symbol},{c.name},{c.holdingsPercent},{c.sharesHeld}");
            }

            File.WriteAllText($".\\etfconstituents_{request.Symbol}", sb.ToString());

        }
    }
}
